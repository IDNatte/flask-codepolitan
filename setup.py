import subprocess
import platform
from distutils import dist
import sys
import os

osSystem = platform.system()
osDistro = platform.version()
conv = platform.python_version()
ubuntuServVersio = platform.dist()
pythonList = ''.join(map(str, conv))

print('[*]Performing System check...\n'
      '[*]Found OS %s %s... \n'
      '[*]Python version %s...\n'
      '[*]Perform database MongoDB check...\n') %(osSystem, osDistro, pythonList)

try:
    subprocess.call(['mongo', '--version'])
    print('[*]Checking database done...')

except:
    print('[*]MongoDB not found...\n[*]Preparation for installing...')
    if '14.04' in ubuntuServVersio:
        os.system('config_system=$(find $PWD -name config-system-ubuntu.14.04.sh) && sudo sh $config_system')
    elif '16.04' in ubuntuServVersio:
        os.system('config_system=$(find $PWD -name config-system-ubuntu.16.04.sh) && sudo sh $config_system')
    else:
        print('[*]Error while executing shell command...')