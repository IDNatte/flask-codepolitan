# PROJECT CODEPOLITAN COMPETITION
First i want to thanks to allah, and for all of my friend who supporting me.
today i'll submit my project. this project is for competition purpose.

## Guide for installation

![](documentation/file-structure.png)

Over here, we have my file structure. So what we got here is:

* Our main folder, it's called `flask-codepolitan`.
* Inside our main folder `flask-codepolitan`, we have another folder
    * First we got folder with name `application`, here i put all of the application file like app.py, static folder for serving static file, template folder, and stuff like that.
    * Second we got folder called `documentation`, and in here i put my image for markdown.
    * Third we got folder called `setup`, in setup folder, i put system config for first use and auto installer application.
    * Fourth we got folder called `venv`, in venv i put all of python virtual env requirements.
    * Fifth we got file called `setup.py`, you should run this script for first time user.
    * And rest of this file you should know what is it for.
    
1 if you wan't to configure this web, first you must ensure that in your's server already installed python3 or higher python3 version and also mongoDB for the database.


2 if you not have mongoDB or python3, you can run `setup.py` for installing database or just only installing python3, with using command `python setup py` on your's machine.

3 after that, you can just sit down and just relax. wait until process done.

4 if you're done, you can type `source venv/bin/activate` from your's console. and then for the last step is type `pip install -r requirements.txt`.

5 and when it's done, you can type `python3 application/app.py` for start the server. and it's done.


yes simple right, so here is my screenshot for the web.
 
 ![](documentation/documentation-1.png)
 
 * this is when you in 'guest mode'.
 
 ![](documentation/documentation-2.png)
 
 * this is the login page.
 
 ![](documentation/documentation-3.png)
 
 * this is how admin page look like.
 
 ![](documentation/documentation-4.png)
 
 * this is the admin's dashboard.
 
 ![](documentation/documentation-5.png)
 
 * this is the register page
 
 ![](documentation/documentation-6.png)
 
 * this is user page look like
 
 ![](documentation/documentation-7.png)
 
 * this is how user dashboard look like
 
 ![](documentation/documentation-8.png)
 
 * upps post not found
 
 ![](documentation/documentation-9.png)
 
 * you will see this page if you want to create new post
 
 ![](documentation/documentation-10.png)
 
 * read article page over here
 
 ![](documentation/documentation-11.png)
 
 * here the edit page
 
 ![](documentation/documentation-12.png)
 
 * and for the last one we got screenshot for error 403 code
 