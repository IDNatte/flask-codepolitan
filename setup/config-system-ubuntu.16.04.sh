#!/usr/bin/env bash

apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
mkdir /etc/apt/sources.list.d/mongodb-org-3.2.list

echo '[*]OS Codename = xenial xerus'
echo '[*]OS Release Number = 16.04'
echo '[*]Starting configuration, Please wait...'
echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.2.list
apt-get update
apt-get install mongodb-org python3-pip

touch /etc/systemd/system/mongod.service
serviceMongodb_config=$(find $PWD -name mongoAuto-config.txt)

container='/etc/systemd/system/mongodb.service'
cat $serviceMongodb_config >> $container
service mongod start
status=$(service mongod status)
echo $status
echo '[*]Installing Python component...'
apt-get install python-dev python3-dev
echo '[*]System configuration is ready...'