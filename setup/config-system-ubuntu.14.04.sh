#!/usr/bin/env bash

apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
mkdir /etc/apt/sources.list.d/mongodb-org-3.2.list

echo '[*]OS Codename = trusty tahr'
echo '[*]OS Release Number = 14.04'
echo '[*]Starting configuration, Please wait...'
echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.2.list
apt-get update
apt-get install mongodb-org python3-pip
service mongod start
status=$(service mongod status)
echo $status
echo '[*]Installing Python component...'
apt-get install python-dev python3-dev
echo '[*]System configuration is ready...'