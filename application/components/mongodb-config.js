db = connect('localhost:27017/project_codepolitan');
db.getSiblingDB('project_codepolitan');

db.createCollection('user_collection');
db.user_collection.insert({'username':'admin', 'password':'admin', 'user_email':'admin@admin.min', 'user_level':'1', 'user_status':'0'})
db.user_collection.insert({'username':'member', 'password':'member', 'user_email':'oniioniichan@gmail.com', 'user_level':'2', 'user_status':'0'})

db.createCollection('blog_collection');
db.blog_collection.createIndex({'post_content':'text'});