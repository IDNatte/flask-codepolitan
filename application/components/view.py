from components import blogContent
from components import dbhandler
from components import app
from components import userLogin
from datetime import timedelta
from pymongo import ASCENDING
from pymongo import DESCENDING
from flask import url_for
from flask import render_template
from flask import Session
from flask import session
from flask import redirect
from flask import request
import time

@app.before_request
def sessionPerm():
    session.permanent = True
    resetTime = app.permanent_session_lifetime = timedelta(hours=24)

@app.route('/index/page')
@app.route('/')

def index():
    if session.get('logged_in'):
        dataUname = session.get('username')
        sessionAdminAuth = userLogin.sessionAdminManager(dataUname)
        if sessionAdminAuth == True:
            return redirect(url_for('admin'))
        else:
            return redirect(url_for('member'))
    else:
        content = blogContent.find().count()
        contentCheck = content
        data = blogContent.find().sort('post_title', ASCENDING)
        return render_template('index.html', data=data, contentCheck=contentCheck, next=next)
           
@app.route('/write_posts')
def input():
    if session.get('logged_in'):
        data = blogContent.blog_collection.find()
        return render_template('input.html', data=data)
    else:
        return redirect(url_for('login'))

@app.route('/process', methods=['POST'])
def process():
    title = request.form.get('posts_title')
    content = request.form.get('posts_content')
    desc = request.form.get('posts_descript')
    posted_by = session.get('username')
    date_posts = time.strftime("%x")
    time_posts = time.strftime("%X")
    insert = blogContent.insert({'post_title':title, 'post_desc':desc, 'post_content':content, 'date_post':date_posts, 'time_posts':time_posts, 'posted_by':posted_by})
    return redirect('/')

@app.route('/delete/', methods=['GET', 'POST'])
def delete():
    if session.get('logged_in'):
        posts_title = request.form.get('delete_title')
        post_id = request.form.get('post_id')
        operation = dbhandler.DatabaseHandler()
        databaseOperation = operation.databaseBlogDeleteItem(post_id, posts_title)
        return redirect('/')
    else:
        return redirect(url_for('error'))

@app.route('/content/<posts_title>/')
def Edit(posts_title):
	data = blogContent.find_one({'post_title':posts_title})
	return render_template('edit.html', data=data)

@app.route('/content/<posts_title>/edit', methods = ['POST'])
def Edit_process(posts_title):
	title = request.form.get('posts_title')
	content = request.form.get('posts_content')
	desc = request.form.get('posts_descript')
	limiter = posts_title
	dbset = blogContent.update({'post_title':limiter}, {'$set':{'post_title':title, 'post_desc':desc, 'post_content':content}})
	return redirect('/')

@app.route('/content/<posts_title>/read/')
def readarticle(posts_title):
	data = blogContent.find_one({'post_title':posts_title})
	return render_template('readarticle.html', data=data)

@app.route('/content/search', methods=['POST'])
def searchcontent():
    dataSearch = request.form.get('search_content')
    result = blogContent.find({'$text':{'$search':dataSearch}})
    resultData = result.count()
    checkContent = bool(resultData)
    checkUserLevel = userLogin.userLevel(session.get('username'))
    if checkContent == True:
        return render_template('searchres.html', result=result, checkUserLevel=checkUserLevel)
    elif checkContent == False:
        return render_template('file404.html', dataSearch=dataSearch)

        
@app.route('/login', methods = ['GET', 'POST'])
def login():
    username = request.form.get('user_name')
    passwd = request.form.get('user_passwd')
    if request.method == 'POST':
        userAdminAuth = userLogin.checkUserAdmin(username, passwd)
        userMemberAuth = userLogin.checkUserMember(username, passwd)
        dataStatusAuth = userLogin.checkStatus(username, '1')
        if not session.get('logged_in'):
            if userAdminAuth == True and userMemberAuth == False:
                userLogin.setLoginStatus(username)
                if dataStatusAuth == False:
                    session['username'] = username
                    session['logged_in'] = True
                    return redirect(url_for('admin'))
                elif dataStatusAuth == True:
                    error3 = 'User with username "%s" has been login in other device' %(username)
                    return render_template('login.html', error3=error3)
    
            elif userAdminAuth == False and userMemberAuth == True:
                userLogin.setLoginStatus(username)
                if dataStatusAuth == False:
                    session['username'] = username
                    session['logged_in'] = True
                    return redirect(url_for('member'))
                elif dataStatusAuth == True:
                    error3 = 'User with username "%s" has been login in other device' %(username)
                    return render_template('login.html', error3=error3)
                
            else:
                error2 = 'Invalid username and password'
                return render_template('login.html', error2=error2)
        else:
            error1 = 'You has been logged in as %s' % (session.get('username'))
            return render_template('login.html', error1=error1)
            
    return render_template('login.html')

@app.route('/admin')
def admin():
    if session.get('logged_in'):
        uname = session.get('username')
        content = blogContent.find().count()
        userAuth = userLogin.userLevel(uname)
        contentCheck = content
        data = blogContent.find().sort('_id', DESCENDING)
                
        return render_template('index.html', uname=uname, data=data, contentCheck=contentCheck, userAuth=userAuth)
    else:
        return redirect(url_for('error'))

@app.route('/user')
def member():
    if session.get('logged_in'):
        uname = session.get('username')
        data = blogContent.find().sort('_id', DESCENDING)
        content = blogContent.find().count()
        userAuth = userLogin.userLevel(uname)
        contentCheck = content
                
        return render_template('index.html', uname=uname, data=data, contentCheck=contentCheck, userAuth=userAuth)
    else:
        return redirect(url_for('error'))

@app.route('/logout')
def logout():
    username = session.get('username')
    userLogin.resetLoginStatus(username)
    session.pop('logged_in', None)
    session.pop('forbidden_login_member', None)
    session.pop('forbidden_login_admin', None)
    session.clear()
    return redirect(url_for('index'))

@app.route('/error')
def error():
    return render_template('error403.html')

@app.route('/u/<user_name>')
def user(user_name):
    if session.get('logged_in'):
        dataCheckAuthLevel = userLogin.sessionAdminManager(user_name)
        if dataCheckAuthLevel == True:
            dataPost = blogContent.find().sort('_id', DESCENDING)
            dataUser = userLogin.user_collection.find()
            checkContent = blogContent.find().count()
            userCheck = userLogin.userLevel(user_name)
            userCount = userLogin.user_collection.find().count()
            return render_template('dashboard.html', dataUser=dataUser, dataPost=dataPost, checkContent=checkContent, userCheck=userCheck, userCount=userCount)
        else:
            mypost = blogContent.find().sort('_id', DESCENDING)
            user =session.get('username')
            return render_template('dashboard.html', mypost=mypost, user=user)
    else:
        return redirect(url_for('error'))
  
@app.route('/regist', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        regist_username = request.form.get('user_name')
        regist_passwd = request.form.get('user_password')
        regist_mail = request.form.get('user_mail')
        checkUserData = userLogin.checkUserMember(regist_username, regist_passwd)
        conf_passwd = request.form.get('user_passwd_confirm')
        if conf_passwd == regist_passwd:
            if checkUserData == True:
                error = 'Account has been registered'
                return render_template('regist.html', error=error)
            else:
                dataRegist = userLogin.userRegist(regist_username, regist_passwd, regist_mail)
                success = 'Register account success'
                return render_template('login.html', success=success)
        else:
            error2 = 'Password not match'
            return render_template('regist.html', error2=error2)
    return render_template('regist.html')
    
@app.route('/u/delete', methods=['GET', 'POST'])
def deluser():
    userGet = request.form.get('delete_user')
    emailGet = request.form.get('user_email')
    userDeleteAccount = userLogin.delUser(userGet, emailGet)
    return redirect('/')

