from flask import *
from components import dbhandler

app = Flask(__name__)
app.secret_key = 'rcjGEPekTkrfdGyx3kKgGTDX6fTZrX82hUAzm4LDptLeUE6Y89RQwXcq8JpxBD4b'
databaseOperation = dbhandler.DatabaseHandler()
blogContent = databaseOperation.databaseBlogHandler()
userLogin = dbhandler.DatabaseUserHandler()