from pymongo import MongoClient
from bson import ObjectId
import os

class DatabaseHandler():
    def __init__(self):
        self.mongoDB = MongoClient()
        self.database =self.mongoDB.project_codepolitan
        dbCheckExistence = self.mongoDB.database_names()
        print('[*]Performing database inspection...\n')
        if 'project_codepolitan' in dbCheckExistence:
            print('[*]database is exists, passing to next argument...')
            pass
        else:
            print('[*]Database is not exists, performing argument... ')
            self.createConfig()

    # def databaseUserHandler(self):
    #     user = self.database.user_collection
    #     return user

    def databaseBlogHandler(self):
        blog = self.database.blog_collection
        return blog

    def databaseBlogDeleteItem(self, post_id, title):
        delete = self.database.blog_collection.remove({'_id':ObjectId(post_id),'post_title':title})
        return delete

    def createConfig(self):
        os.system('config_db=$(find $PWD -name mongodb-config.js) && mongo < $config_db')

class DatabaseUserHandler():
    def __init__(self):
        self.mongoDB = MongoClient()
        self.database =self.mongoDB.project_codepolitan

    def checkUserAdmin(self, username, password):
        self.user_collection = self.database.user_collection
        self.result = self.user_collection.find_one({'username':username, 'password':password, 'user_level':'1'}, {'_id':0, 'username':1, 'password':1})
        return bool(self.result)
    
    def checkUserMember(self, username, password):
        self.user_collection = self.database.user_collection
        self.result = self.user_collection.find_one({'username':username, 'password':password, 'user_level':'2'}, {'_id':0, 'username':1, 'password':1})
        return bool(self.result)
    
    def sessionAdminManager(self, sessionIdentifier):
        self.user_collection = self.database.user_collection
        self.result = self.user_collection.find_one({'username':sessionIdentifier, 'user_level':'1'})
        return bool(self.result)
    
    def findUserInfo(self, sessionIdentifier):
        self.user_collection = self.database.user_collection
        self.result = self.user_collection.find_one({'username':sessionIdentifier})
        return bool(self.result)
    
    def setLoginStatus(self, username):
        self.user_collection = self.database.user_collection
        self.result = self.user_collection.update({'username':username}, {'$set':{'user_status' : "1"}})
    
    def resetLoginStatus(self, username):
        self.user_collection = self.database.user_collection
        self.result = self.user_collection.update({'username':username}, {'$set':{'user_status' : "0"}})
    
    def checkStatus(self, username, status):
        self.user_collection = self.database.user_collection
        self.result = self.user_collection.find_one({'username':username, 'user_status': status})
        return bool(self.result)

    def delUser(self, username, email):
        self.user_collection = self.database.user_collection
        self.result = self.user_collection.delete_one({'username':username, 'user_email':email})
        return self.result
   
    def userLevel(self, value):
        self.user_collection = self.database.user_collection
        self.result = self.user_collection.find_one({'username':value, 'user_level':'1'})
        return bool(self.result)
    
    def userRegist(self, username, password, email):
        self.user_collection = self.database.user_collection
        self.insert = self.user_collection.insert({'username':username, 'password':password, 'user_email':email, 'user_level': '2', 'user_status': '0'})