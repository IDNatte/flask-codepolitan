from components import view
from components import app


if __name__ == '__main__':
    app.run(host = '0.0.0.0', port = 7720, debug = True)